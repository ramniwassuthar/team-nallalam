package com.business.enquirymanagement.sessions;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

public class SessionManager {
    private static final String TAG = "SessionManager";

    public static final String KEY_UserID = "UserID";
    public static final String KEY_NAME = "NAME";
    public static final String KEY_TOKEN = "Name";
    public static final String KEY_TIMESTAMP = "TIMESTAMP";
    public static final String KEY_TIMEZONE= "TIMEZONE";



    // User Details after logoin

    private static final String PREF_NAME = "EnquirManagement";
    public static final String IS_Token_Valid = "IsTokanValid";


    int PRIVATE_MODE = 0;
    private SharedPreferences pref;
    private Editor editor;
    private Context _context;

    @SuppressLint("CommitPrefEdits")
    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    /**
     * Create login session
     */

    public void createLoginSession(String name, String token,String timestamp, String timezone) {


        try {
            editor.putString(KEY_NAME, name);
            editor.putString(KEY_TOKEN, token);
            editor.putString(KEY_TIMESTAMP, timestamp);
            editor.putString(KEY_TIMEZONE, timezone);

            editor.putBoolean(IS_Token_Valid, true);

            // editor.apply();
            editor.commit();
        } catch(Throwable t) {
            Log.d(TAG, "createLoginSession: "+t);
        }


    }







    public void setValuesSession(String setKey, String setValue) {
        editor.putString(setKey, setValue);
        editor.apply();
    }

    public void setValuesSession(String setKey, boolean setValue) {
        editor.putBoolean(setKey, setValue);
        editor.apply();
    }

    public String getValuesSession(String getKey) {

        return pref.getString(getKey, "");
    }

    public boolean getValuesSessionBoolean(String getKey) {

        return pref.getBoolean(getKey, false);
    }



    public void logoutUser() {
        editor.clear();
        editor.commit();
    }

    /**
     * Quick check for login
     **/
    public boolean isLoggedIn() {
        return pref.getBoolean(IS_Token_Valid, false);
    }


}

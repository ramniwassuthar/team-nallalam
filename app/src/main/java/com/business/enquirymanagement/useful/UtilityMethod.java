package com.business.enquirymanagement.useful;


import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.business.enquirymanagement.R;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import static android.content.Context.DOWNLOAD_SERVICE;


/**
 * Created by RN Suthar 20/Aug/2018.
 */
public class UtilityMethod {
    private static final String TAG = "UtilityMethod";
    private static Context ctx;


    public static void goNextClass(Context context, Class className) {

        Intent intent = new Intent(context, className);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void showAlertBoxWithIntent(final Context context, String msg, final Class sendClass) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(true)
                .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        Intent i = new Intent(context, sendClass);
                        context.startActivity(i);
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.show();
    }

    public static void showAlertBox(Context context, String msg) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(true)
                .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public static void showAlertBackgroundThread(final Context context, final String massage, boolean isToast) {
        ctx = context;
        Message msg = new Message();
        msg.obj = massage.toString();

        if (isToast) {
            responseHandlerToast.sendMessage(msg);
        } else {
            responseHandler.sendMessage(msg);
        }


    }

    static final Handler responseHandlerToast = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {

            String Message = msg.obj.toString().substring(0, (msg.obj.toString().indexOf("#")));
            String imageSring = msg.obj.toString().substring((msg.obj.toString().indexOf("#") + 1));

            int icon = getDrawableByName(ctx, imageSring);
            showCoustomToast(Message, ctx, icon);

        }
    };

    public static void showCoustomToast(final String massage, Context mContext, int icon) {

        Activity activity = (Activity) mContext;
        LayoutInflater inflater = activity.getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast, null);

        ImageView image = layout.findViewById(R.id.image);
        image.setImageResource(icon);
        TextView text = layout.findViewById(R.id.text);
        text.setText(massage);

        Toast toast = new Toast(mContext);
        toast.setGravity(Gravity.BOTTOM, 0, 100);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();

    }

    static final Handler responseHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctx);
            alertDialogBuilder
                    .setMessage(msg.obj.toString())
                    .setCancelable(true)
                    .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });

            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();

            alertDialog.show();


            // Toast.makeText(RegistrationActivity.this, "Data :- " + message.obj, Toast.LENGTH_SHORT).show();
        }
    };


    public static void showToast(Context context, String message) {

        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

    }



    // Method to show alert dialog
    public static void showAlert(String message, Context context) {


        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder
                .setMessage(message)
                .setCancelable(true)
                .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.show();


    }


    public static String setCapWord(String capString) {
        return capString.substring(0, 1).toUpperCase() + capString.substring(1);
    }

    public static void clearViewFouces(final EditText editText) {
        editText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    editText.clearFocus();
                }
                return false;
            }
        });

    }


    public static String toTitleCase(String givenString) {
        String[] arr = givenString.split(" ");
        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < arr.length; i++) {
            sb.append(Character.toUpperCase(arr[i].charAt(0)))
                    .append(arr[i].substring(1)).append(" ");
        }
        return sb.toString().trim();
    }


    /**
     * Get Array By Name
     */

    public static int getArrayByName(Context context, String resourceName) {
        int resourceId = -1;
        try {
            resourceId = context.getResources().getIdentifier(resourceName, "array", context.getPackageName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resourceId;
    }

    /**
     * Get Drawable By Name
     */

    public static int getDrawableByName(Context context, String resourceName) {
        int resourceId = -1;
        try {
            resourceId = context.getResources().getIdentifier(resourceName, "drawable", context.getPackageName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resourceId;
    }

    /**
     * Get Color By Name
     */

    public static int getColorByName(Context context, String resourceName) {
        int resourceId = -1;
        try {
            resourceId = context.getResources().getIdentifier(resourceName, "color", context.getPackageName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resourceId;
    }


    public static double calculateGST(double money, double gst) {
        double calculateValue = 0;
        DecimalFormat df2 = new DecimalFormat(".##");
        if (money > 0 && gst > 0) {

            calculateValue = (money / 100) * gst;
        }
        // df2.setRoundingMode(RoundingMode.UP);

        return Double.parseDouble(df2.format(calculateValue));
    }

    public static String FormatedDate(String date, String oldFormat) {
        String formatedDate = "";

        // String oldFormat = "yyyy-MM-dd hh:mm:ss";
        //String newFormat = "dd MMM yyyy hh:mm:ss";
        String newFormat = "dd MMM yyyy";


        SimpleDateFormat dateFormat = new SimpleDateFormat(oldFormat);
        Date myDate = null;
        try {
            myDate = dateFormat.parse(date);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat timeFormat = new SimpleDateFormat(newFormat);
        formatedDate = timeFormat.format(myDate);


        return formatedDate;
    }



    public static void downloadFile(Context mContext, String url, String fileName) {

        DownloadManager.Request r = new DownloadManager.Request(Uri.parse(url));
        r.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);
        r.allowScanningByMediaScanner();
        r.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        DownloadManager dm = (DownloadManager) mContext.getSystemService(DOWNLOAD_SERVICE);
        dm.enqueue(r);
    }


    public static String SetDecimalDigit(double totalItemPrice) {
        return new DecimalFormat("##.##").format(totalItemPrice);
    }

    public static void ResetAllConstant() {

    }

    public static void showCoustomToastError(Context mContext,String massage) {

        if (massage.equals("")){
            massage=mContext.getResources().getString(R.string.MsgNotResponse)+"  ";
        }

        Activity activity = (Activity) mContext;
        LayoutInflater inflater = activity.getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast, null);

        ImageView image = layout.findViewById(R.id.image);
        image.setImageResource(R.drawable.toast_error);
        TextView text = layout.findViewById(R.id.text);
        text.setText(massage);

        Toast toast = new Toast(mContext);
        toast.setGravity(Gravity.BOTTOM, 0, 100);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();


    }
}

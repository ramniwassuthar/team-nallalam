package com.business.enquirymanagement.useful;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.util.Log;


/**
 * Created by Kushal on 27-May-16.
 */
public class MyApp extends Application {
 static Context context;
    @Override
    public void onCreate() {
        super.onCreate();
        context=getApplicationContext();
        Log.d("My App Called","My App Called");

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);

    }

    public static Context getContext(){
        return context;
    }
}

package com.business.enquirymanagement.useful;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;



/**
 * Created by sunarc-13 on 16/9/15.
 */
public class alert {

    public static void show_ok_only_no_task(final Context ctx, String title, String message)
    {
        final AlertDialog dialog = new AlertDialog.Builder(ctx)
               .setTitle(title)
        .setMessage(message)
       .setCancelable(true)
       .setPositiveButton("OK", new DialogInterface.OnClickListener() {
           @Override
           public void onClick(DialogInterface dialog, int which) {
               // TODO Auto-generated method stub
               dialog.dismiss();
           }
       }).create();
              dialog.show();

    }




    public static void show_ok_cancel_specific_with_clear_prefs(final Context ctx, String title, String message, final Intent i)
    {
        final AlertDialog dialog = new AlertDialog.Builder(ctx)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(true)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                       // new Defaults().clearPreferences();
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        ctx.startActivity(i);
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();

            dialog.show();

    }

    public static void show_ok_go_specific(final Context ctx, String title, String message, final Intent i)
    {
        final AlertDialog dialog = new AlertDialog.Builder(ctx)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(true)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                        ((Activity)ctx).finish();
                        ctx.startActivity(i);
                    }
                })
                .create();
            dialog.show();
     }


    public static void show_update_later_go_specific(final Context ctx, String title, String message, final Intent i)
    {
        final AlertDialog dialog = new AlertDialog.Builder(ctx)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(true)
                .setPositiveButton("Update Now", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                        ((Activity)ctx).finish();
                        ctx.startActivity(i);
                    }
                })
                .setNegativeButton("Later",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                      //  ((Activity)ctx).finish();
                       // ctx.startActivity(i);
                    }
                })
                .create();
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

    }



}

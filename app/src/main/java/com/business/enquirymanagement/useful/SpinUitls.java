package com.business.enquirymanagement.useful;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.business.enquirymanagement.R;

import java.util.ArrayList;
import java.util.List;



/**
 * Created by user on 21-Mar-18.
 */

public class SpinUitls {
    private static final String TAG = "SpinUitls";
    public static final String CONST_GROUP = "GROUP";

    public static String GROUP_SPINNER_ID = "";


    public static void setSpinner(final Context mContext, final Spinner spinner, final List<SpinPojo> spinPojosList, String defaultValueToSet, final String type) {

        List<String> itemStringList = new ArrayList<>();
        for (int i = 0; i < spinPojosList.size(); i++) {
            itemStringList.add(spinPojosList.get(i).getName());

        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, R.layout.row_sppiner, itemStringList);
        spinner.setAdapter(adapter);

        if (!defaultValueToSet.equals("")) {
            for (int i = 0; i < spinPojosList.size(); i++) {
                if (defaultValueToSet.equals(spinPojosList.get(i).getID())) {
                    spinner.setSelection(i);
                    switch (type) {

                        case CONST_GROUP:
                            GROUP_SPINNER_ID = spinPojosList.get(i).getID();
                            Log.d(TAG, "GROUP_SPINNER_ID_UPDATE: " + GROUP_SPINNER_ID);
                            break;

                    }

                    break;
                }
            }
        }

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position != 0) {

                    String item = parent.getItemAtPosition(position).toString();


                    switch (type) {
                        case CONST_GROUP:
                            GROUP_SPINNER_ID = spinPojosList.get(position).getID();
                            Log.d(TAG, "GROUP_SPINNER_ID: " + GROUP_SPINNER_ID);
                            break;

                    }

                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public static class SpinPojo {

        public String ID;
        public String Name;

        public SpinPojo() {
        }

        public SpinPojo(String ID, String name) {
            this.ID = ID;
            Name = name;
        }

        public String getID() {
            return ID;
        }

        public void setID(String ID) {
            this.ID = ID;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }
    }

}

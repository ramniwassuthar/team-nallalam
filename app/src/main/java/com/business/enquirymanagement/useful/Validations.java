package com.business.enquirymanagement.useful;

import android.content.Context;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;


public class Validations {

    public static boolean isValidMobile(EditText mobile, String error) {

        boolean val = false;
        String mob = mobile.getText().toString().trim();


        if (mob.length() == 10 && !mob.isEmpty()) {

            val = true;
        } else {

            mobile.setError(error);
            mobile.requestFocus();

        }

        return val;
    }


    public static boolean isValidPin(EditText pin) {

        boolean val = false;
        String postal = pin.getText().toString().trim();


        if (postal.length() == 6 && !postal.isEmpty()) {

            val = true;
        } else {

            pin.setError("Invalid pin code");
            pin.requestFocus();

        }

        return val;
    }


    public static boolean isValidEmail(EditText email, String error) {
        boolean val = false;


        if (android.util.Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()) {

            val = true;

        } else {

            email.setError(error);
            email.requestFocus();
        }

        return val;
    }


    public static boolean isValidName(EditText name) {

        boolean val = false;
        String mob = name.getText().toString().trim();


        if (!mob.isEmpty()) {

            val = true;
        } else {

            name.setError("Invalid name");
            name.requestFocus();

        }

        return val;
    }

    public static boolean isValidSpinner(Spinner spinner,String Err,Context mContext) {

        boolean val = false;
        int postion = spinner.getSelectedItemPosition();


        if (postion!=0) {

            val = true;
        } else {

            UtilityMethod.showAlertBox(mContext,Err);

        }

        return val;
    }


    public static boolean isBlank(EditText editText,String error) {

        boolean val = false;
        String value = editText.getText().toString().trim();


        if (!value.isEmpty()) {

            val = true;
        } else {

            editText.setError(error);
            editText.requestFocus();

        }

        return val;
    }


    public static boolean isBlankTextView(TextView editText, String error) {

        boolean val = false;
        String value = editText.getText().toString().trim();


        if (!value.isEmpty()) {

            val = true;
        } else {

            editText.setError(error);
            editText.requestFocus();

        }

        return val;
    }


    public static boolean isCnfrmPassword(EditText first,EditText second,String error) {

        boolean val = false;
        String first_value = first.getText().toString().trim();
        String second_value = second.getText().toString().trim();


        if (!first_value.isEmpty()&&!second_value.isEmpty()) {
            if (first_value.equals(second_value)){
                val = true;
            }else {
                second.setError(error);
                second.requestFocus();
            }
        } else {

            second.setError(error);
            second.requestFocus();

        }

        return val;
    }

    public static boolean isEditTextFilled(EditText name, String ErrorMsg) {

        boolean val = false;
        String mob = name.getText().toString().trim();


        if (!mob.isEmpty()) {

            val = true;
        } else {

            name.setError(ErrorMsg);
            name.requestFocus();

        }

        return val;
    }

    public static boolean isStringFilled(String value, String ErrorMsg, Context context) {

        boolean val = false;
        if (!value.isEmpty()) {

            val = true;
        } else {
            UtilityMethod.showAlert(ErrorMsg, context);
        }

        return val;
    }
}

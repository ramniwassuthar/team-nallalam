package com.business.enquirymanagement.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.business.enquirymanagement.R;
import com.business.enquirymanagement.model.EnquiryM;

/**
 * Created by sunarctech on 13/6/18.
 */

public class ReportsActivity extends AppCompatActivity {

    private static final String TAG = "ReportsActivity";
    TextView tvTotalConversion, tvTotalEnq;
    EnquiryM enquiryM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reports);
        tvTotalEnq = findViewById(R.id.tvTotalEnq);
        tvTotalConversion = findViewById(R.id.tvTotalConversion);

        enquiryM = EnquiryM.newInstance();

        int total = enquiryM.enquiryMSCOLD.size()+enquiryM.enquiryMSHOT.size()+enquiryM.enquiryMSWARM.size();

        tvTotalEnq.setText(String.valueOf(total));
        tvTotalConversion.setText(String.valueOf(total));
    }

    public void doBack(View view){
        finish();
    }
}


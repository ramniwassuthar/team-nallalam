package com.business.enquirymanagement.activity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.business.enquirymanagement.R;
import com.business.enquirymanagement.model.EnquiryM;
import com.business.enquirymanagement.model.ProductM;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class EnquirySubmitActivity extends AppCompatActivity {

    private static final String TAG = "EnquirySubmitActivity";
    EditText ed_addr, ed_name, ed_mobile, ed_remark, ed_activityplace, ed_followupdate;
    Spinner spinner, spProducts;
    String customerName, address, mobileNo, product, remark, enquiryType, activityPlace, followupDate, attempt;
    ArrayList<String> enquiryTypes = new ArrayList<>();
    ArrayList<String> attempts = new ArrayList<>();
    Calendar mCalender;
    TextView tvAttmepts;
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enquiry_submit);
        init();
    }

    private void init() {
        ed_addr = findViewById(R.id.ed_addr);
        ed_name = findViewById(R.id.ed_name);
        ed_mobile = findViewById(R.id.ed_mobile);
        spProducts = findViewById(R.id.spProducts);
        ed_remark = findViewById(R.id.ed_remark);
        ed_followupdate = findViewById(R.id.ed_followupdate);
        ed_activityplace = findViewById(R.id.ed_activityplace);
        spinner = findViewById(R.id.spinner);
        tvAttmepts = findViewById(R.id.tvAttmepts);

        enquiryTypes.add(mContext.getResources().getString(R.string.HOT));
        enquiryTypes.add(mContext.getResources().getString(R.string.COLD));
        enquiryTypes.add(mContext.getResources().getString(R.string.WARM));


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_row, enquiryTypes);
        spinner.setAdapter(adapter);

        attempts.add("1");
        attempts.add("2");
        attempts.add("3");

       /* ArrayAdapter<String> spAdap = new ArrayAdapter<String>(this,R.layout.spinner_row,attempts);
        spAttempts.setAdapter(spAdap);*/

        ProductM productM = ProductM.getInstance();
        ArrayList<String> proNames = new ArrayList<>();
        for (ProductM productM1 : productM.productMS) {
            proNames.add(productM1.proName);
        }

        ArrayAdapter<String> spProAdap = new ArrayAdapter<String>(this, R.layout.spinner_row, proNames);
        spProducts.setAdapter(spProAdap);

        mCalender = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                mCalender.set(Calendar.YEAR, year);
                mCalender.set(Calendar.MONTH, monthOfYear);
                mCalender.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                updateLabel();
            }
        };

        ed_followupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(EnquirySubmitActivity.this, listener, mCalender.get(Calendar.YEAR)
                        , mCalender.get(Calendar.MONTH), mCalender.get(Calendar.DAY_OF_MONTH)).show();
            }
        });


    }

    public void doBack(View view) {
        finish();
    }

    public void submitForm(View view) {
        customerName = ed_name.getText().toString();
        address = ed_addr.getText().toString();
        mobileNo = ed_mobile.getText().toString();
        product = (String) spProducts.getSelectedItem();
        remark = ed_remark.getText().toString();
        activityPlace = ed_activityplace.getText().toString();
        enquiryType = (String) spinner.getSelectedItem();
        followupDate = ed_followupdate.getText().toString();
        attempt = tvAttmepts.getText().toString();

        Log.e(TAG, "enquiryType===" + enquiryType);

        if (isFormValidated()) {
            EnquiryM enquiryM = EnquiryM.newInstance();
            EnquiryM enqM = new EnquiryM(customerName, address, mobileNo, product, remark, enquiryType, activityPlace,
                    followupDate, attempt);
            if (enquiryType.equalsIgnoreCase("HOT")) {
                enquiryM.enquiryMSHOT.add(enqM);
                enquiryM.totalEnquires.add(enqM);
            } else if (enquiryType.equalsIgnoreCase("WARM")) {
                enquiryM.enquiryMSWARM.add(enqM);
                enquiryM.totalEnquires.add(enqM);
            } else {
                enquiryM.enquiryMSCOLD.add(enqM);
                enquiryM.totalEnquires.add(enqM);
            }
            Intent intent = new Intent();
            Log.e(TAG, "enquiryM.enquiryType===" + enquiryType);
            intent.putExtra("enquiryType", enquiryType);
            setResult(RESULT_OK, intent);
            this.finish();
        }

    }

    private boolean isFormValidated() {
        if (customerName.isEmpty()) {
            ed_name.setError("Customer name is mandatory.");
            ed_name.requestFocus();
            return false;
        }
        if (mobileNo.isEmpty()) {
            ed_mobile.setError("Mobile number is mandatory.");
            ed_mobile.requestFocus();
            return false;
        }
        if (mobileNo.length() != 10) {
            ed_mobile.setError("Mobile number must be 10 digits.");
            ed_mobile.requestFocus();
            return false;
        }
        return true;
    }

    private void updateLabel() {
        String myFormat = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        ed_followupdate.setText(sdf.format(mCalender.getTime()));

    }
}

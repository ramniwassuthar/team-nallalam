package com.business.enquirymanagement.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.business.enquirymanagement.R;
import com.business.enquirymanagement.model.LoginResponse;
import com.business.enquirymanagement.networks.ApiClient;
import com.business.enquirymanagement.networks.ApiInterface;
import com.business.enquirymanagement.networks.AppGlobal;
import com.business.enquirymanagement.networks.ProgressDialogLoader;
import com.business.enquirymanagement.sessions.SessionManager;
import com.business.enquirymanagement.useful.UtilityMethod;
import com.business.enquirymanagement.useful.Validations;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Response;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by sunarctech on 3/7/18.
 */

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "LoginActivity";
    EditText edtEmail, edtPassword;
    Button but_login;
    Context mContext;
    ApiInterface apiInterface;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mContext = this;
        sessionManager = new SessionManager(mContext);
        findViews();

    }

    private void findViews() {
        edtEmail = findViewById(R.id.edEmail);
        edtPassword = findViewById(R.id.edPassword);
        but_login = findViewById(R.id.but_login);

        edtEmail.setText("sales_person@mailinator.com");
        edtPassword.setText("12345678");

        but_login.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.but_login:

                if (Validations.isValidEmail(edtEmail, mContext.getResources().getString(R.string.ErrEmail)) &&
                        Validations.isBlank(edtPassword, mContext.getResources().getString(R.string.ErrPassword))) {
                    login();
                }

                break;

        }
    }

    private void login() {

        Map<String, String> map = new HashMap<>();

        map.put("email", edtEmail.getText().toString());
        map.put("password", edtPassword.getText().toString());


        ProgressDialogLoader.progressdialog_creation((Activity) mContext, mContext.getString(R.string.please_wait));

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Observable<Response<LoginResponse>> call = apiInterface.login(map);
        call.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<LoginResponse>>() {
                    @Override
                    public void onCompleted() {
                        if (ProgressDialogLoader.isDalogShowing()) {
                            ProgressDialogLoader.progressdialog_dismiss();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (ProgressDialogLoader.isDalogShowing()) {
                            ProgressDialogLoader.progressdialog_dismiss();
                        }
                        UtilityMethod.showCoustomToastError(mContext, "");
                    }

                    @Override
                    public void onNext(Response<LoginResponse> response) {

                        try {
                            if (ProgressDialogLoader.isDalogShowing()) {
                                ProgressDialogLoader.progressdialog_dismiss();
                            }

                            if (response.isSuccessful()) {
                                if (response.body().getMeta().isSuccess()) {

                                    AppGlobal.Tokan=response.body().getData().getToken();
                                    Log.d(TAG, "onCreate: "+AppGlobal.Tokan);

                                    sessionManager.createLoginSession(response.body().getData().getName(),
                                            response.body().getData().getToken(),
                                            String.valueOf(response.body().getTimestamp()),
                                            response.body().getTimezone());

                                    Intent loginIntent = new Intent(LoginActivity.this, FollowupbaseActivity.class);
                                    loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(loginIntent);
                                    finish();


                                } else {
                                    UtilityMethod.showCoustomToastError(mContext, response.body().getMeta().getMessage());
                                }
                            } else {
                                UtilityMethod.showCoustomToastError(mContext, "");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }
                });

    }
}

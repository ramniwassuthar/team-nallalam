package com.business.enquirymanagement.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.business.enquirymanagement.R;
import com.business.enquirymanagement.fragment.EnquiryTypeBaseFrag;
import com.business.enquirymanagement.fragment.ProductBaseFrag;
import com.business.enquirymanagement.model.ProductM;

import static com.business.enquirymanagement.adapters.EnquiryTypeAdap.UPDATE_FOLLOWUP;

/**
 * Created by sunarctech on 13/6/18.
 */

public class FollowupbaseActivity extends AppCompatActivity {

    private static final String TAG = "FollowupbaseActivity";
    FrameLayout container ;
    BottomNavigationView bottomNavigationView ;
    EnquiryTypeBaseFrag enqBaseFrag;
    ProductBaseFrag productBaseFrag ;
    FragmentManager fm ;
    Fragment active ;
    ProductM productM ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_followup_base);
        container = findViewById(R.id.container);
        bottomNavigationView = findViewById(R.id.bottomNavigationView);
        fm = getSupportFragmentManager();

        productM = ProductM.getInstance();
        productM.productMS.clear();

        productM.productMS.add(new ProductM(R.drawable.honda_cb_hornet,"Honda CB Hornet","₹ 60,628",
                "71.00 Kmpl","109.2 CC"));
        productM.productMS.add(new ProductM(R.drawable.honda_cb_unicorn_160,"Honda CB Unicorn","₹ 75,555",
                "60.00 Kmpl","70.0 CC"));
        productM.productMS.add(new ProductM(R.drawable.honda_cd_110,"Honda CD 110","₹ 65,000",
                "75.00 Kmpl","90.8 CC"));
        productM.productMS.add(new ProductM(R.drawable.honda_livo,"Honda Livo","₹ 60,000",
                "74.00 Kmpl","106.0 CC"));
        productM.productMS.add(new ProductM(R.drawable.honda_x_blade,"Honda X Blade","₹ 70,665",
                "66.00 Kmpl","100.5 CC"));

        enqBaseFrag = EnquiryTypeBaseFrag.newInstance();
        productBaseFrag = ProductBaseFrag.newInstance();



        addHideFragments();


       bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.menuEnquiryType:
                        showHideFragments(active,enqBaseFrag);
                        active = enqBaseFrag ;
                        break;
                    case R.id.menuProduct:
                        showHideFragments(active,productBaseFrag);
                        active = productBaseFrag ;
                        break;

                }
                return true;
            }
        });
    }

    private void addHideFragments(){
        fm.beginTransaction().add(R.id.container,productBaseFrag,"ShoppingBaseFrag").hide(productBaseFrag).commit();
        fm.beginTransaction().add(R.id.container,enqBaseFrag,"ShoppingBaseFrag").commit();
        active = enqBaseFrag;

    }
    private void showHideFragments(Fragment hide, Fragment show){
        fm.beginTransaction().hide(hide).show(show).commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == UPDATE_FOLLOWUP){
            if(resultCode == RESULT_OK){
                enqBaseFrag.updateList();
            }
        }
    }
}
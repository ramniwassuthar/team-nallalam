package com.business.enquirymanagement.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.business.enquirymanagement.R;
import com.business.enquirymanagement.model.EnquiryM;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by sunarctech on 13/6/18.
 */

public class FollowupDetailActivity extends AppCompatActivity {

    private static final String TAG = "FollowupDetailActivity";
    TextView tvCusName,tvAddr,tvMob,tvProduct,tvEnqType,tvActPlace,tvRemark,tvFollowup,tvAttmepts2,tvAttmepts3;
    EditText edRemark2,edFollowup2,edRemark3,edFollowup3;
    Spinner spinnerStatus;
    EnquiryM enquiryM ;
    Calendar mCalender;
    int position ;
    LinearLayout repeatedLinear2 ;
    ImageView photo ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_followup_detail);
        enquiryM = (EnquiryM) getIntent().getSerializableExtra("enquiryM1");
        position = getIntent().getIntExtra("position",0);
        Log.e(TAG,"position==="+position);
        Log.e(TAG,"enquiryM.remark==="+enquiryM.remark);
        init();
    }

    public void doBack(View view){
        finish();
    }

    private void init(){
        tvCusName = findViewById(R.id.tvCusName);
        tvAddr = findViewById(R.id.tvAddr);
        tvMob = findViewById(R.id.tvMob);
        tvProduct = findViewById(R.id.tvProduct);
        tvEnqType = findViewById(R.id.tvEnqType);
        tvActPlace = findViewById(R.id.tvActPlace);
        tvRemark = findViewById(R.id.tvRemark);
        tvFollowup = findViewById(R.id.tvFollowup);
        spinnerStatus = findViewById(R.id.spinnerStatus);
        repeatedLinear2 = findViewById(R.id.repeatedLinear2);
        edRemark2 = findViewById(R.id.edRemark2);
        edFollowup2 = findViewById(R.id.edFollowup2);
        edRemark3 = findViewById(R.id.edRemark3);
        edFollowup3 = findViewById(R.id.edFollowup3);
        tvAttmepts2 = findViewById(R.id.tvAttmepts2);
        tvAttmepts3 = findViewById(R.id.tvAttmepts3);

        photo = findViewById(R.id.photo);

        photo.setImageResource(enquiryM.proImage);


        tvCusName.setText(enquiryM.customerName);
        tvAddr.setText(enquiryM.address);
        tvMob.setText(enquiryM.mobileNo);
        tvProduct.setText(enquiryM.product);
        tvEnqType.setText(enquiryM.enquiryType);
        tvActPlace.setText(enquiryM.activityPlace);
        tvRemark.setText(enquiryM.remark);

        Log.e(TAG,"enquiryM.followupDate=="+enquiryM.followupDate);
        tvFollowup.setText(enquiryM.followupDate);

        edRemark2.setText(enquiryM.remark2);
        edFollowup2.setText(enquiryM.followupDate2);

        edRemark3.setText(enquiryM.remark3);
        edFollowup3.setText(enquiryM.followupDate3);
        mCalender = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                mCalender.set(Calendar.YEAR,year);
                mCalender.set(Calendar.MONTH,monthOfYear);
                mCalender.set(Calendar.DAY_OF_MONTH,dayOfMonth);

                updateLabel(edFollowup2);
            }
        };

        edFollowup2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(FollowupDetailActivity.this,listener,mCalender.get(Calendar.YEAR)
                        ,mCalender.get(Calendar.MONTH),mCalender.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        String[] statusArr = new String[]{"Open","Purchased","Lost"};
        ArrayAdapter<String> adap = new ArrayAdapter<String>(this,R.layout.spinner_row,statusArr);
        spinnerStatus.setAdapter(adap);

        /*String[] attemptsArr = new String[]{"1","2","3"};
        ArrayAdapter<String> adap1 = new ArrayAdapter<String>(this,R.layout.spinner_row,attemptsArr);
        spAttempts.setAdapter(adap1);
            Log.e(TAG,"enquiryM.attempts==="+enquiryM.attempts);
        spAttempts.setSelection(Arrays.asList(attemptsArr).indexOf(enquiryM.attempts));*/


        if(enquiryM.attempts.equals("2")){

            // disable the 2nd remark and follow up for editing

            repeatedLinear2.setVisibility(View.VISIBLE);

            disable2Fields();

            final DatePickerDialog.OnDateSetListener listener2 = new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                    mCalender.set(Calendar.YEAR,year);
                    mCalender.set(Calendar.MONTH,monthOfYear);
                    mCalender.set(Calendar.DAY_OF_MONTH,dayOfMonth);

                    updateLabel(edFollowup3);
                }
            };

            edFollowup3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new DatePickerDialog(FollowupDetailActivity.this,listener2,mCalender.get(Calendar.YEAR)
                            ,mCalender.get(Calendar.MONTH),mCalender.get(Calendar.DAY_OF_MONTH)).show();
                }
            });
        }

        if(enquiryM.attempts.equals("3")){

            repeatedLinear2.setVisibility(View.VISIBLE);
            disable2Fields();
            disable3Fields();

        }


    }

    public void updateFollowup(View view){

        if(repeatedLinear2.getVisibility()== View.VISIBLE){

            String remark3 = edRemark3.getText().toString();
            String followupDate3 = edFollowup3.getText().toString();
            String attempts3 = tvAttmepts3.getText().toString();


            Log.e(TAG,"remark3==="+remark3);
            Log.e(TAG,"followupDate3==="+followupDate3);
            Log.e(TAG,"attemps3 =="+attempts3);

            enquiryM.remark3 = remark3;
            enquiryM.followupDate3 = followupDate3 ;
            enquiryM.attempts = attempts3 ;
        }else {
            String remark2 = edRemark2.getText().toString();
            String followupDate2 = edFollowup2.getText().toString();
            String attempts = tvAttmepts2.getText().toString();

            Log.e(TAG,"remark2==="+remark2);
            Log.e(TAG,"followupDate2==="+followupDate2);

            enquiryM.remark2 = remark2 ;
            enquiryM.followupDate2 = followupDate2;
            enquiryM.attempts = attempts ;
        }


        EnquiryM enquiryM1 = EnquiryM.newInstance();

        if(enquiryM.enquiryType.equalsIgnoreCase("HOT")){
            enquiryM1.enquiryMSHOT.set(position,enquiryM);
        }else if(enquiryM.enquiryType.equalsIgnoreCase("WARM")){
            enquiryM1.enquiryMSWARM.set(position,enquiryM);
        }else {
            enquiryM1.enquiryMSCOLD.set(position,enquiryM);
        }

        Toast.makeText(FollowupDetailActivity.this,"You have updated the Followup",Toast.LENGTH_SHORT).show();

        setResult(RESULT_OK);
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                FollowupDetailActivity.this.finish();
            }
        },1000);



    }

    private void updateLabel(EditText ed){
        String myFormat = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        ed.setText(sdf.format(mCalender.getTime()));

    }

    private void disable2Fields(){
        edRemark2.setClickable(false);
        edRemark2.setFocusable(false);
        edRemark2.setFocusableInTouchMode(false);
        edRemark2.setEnabled(false);

        edFollowup2.setClickable(false);
        edFollowup2.setFocusable(false);
        edFollowup2.setFocusableInTouchMode(false);
        edFollowup2.setEnabled(false);
        edFollowup2.setEnabled(false);

    }

    private void disable3Fields(){
        edRemark3.setEnabled(false);
        edRemark3.setFocusableInTouchMode(false);
        edRemark3.setFocusable(false);
        edRemark3.setClickable(false);

        edFollowup3.setClickable(false);
        edFollowup3.setEnabled(false);
        edFollowup3.setFocusable(false);
        edFollowup3.setFocusableInTouchMode(false);
    }


}

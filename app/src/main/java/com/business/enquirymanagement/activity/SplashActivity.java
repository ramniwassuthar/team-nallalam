package com.business.enquirymanagement.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;

import com.business.enquirymanagement.R;
import com.business.enquirymanagement.networks.ApiInterface;
import com.business.enquirymanagement.networks.AppGlobal;
import com.business.enquirymanagement.sessions.SessionManager;

/**
 * Created by sunarctech on 3/7/18.
 */

public class SplashActivity extends AppCompatActivity {

    private static final String TAG = "SplashActivity";
    EditText edtEmail, edtPassword;
    Button but_login;
    Context mContext;
    ApiInterface apiInterface;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mContext = this;
        sessionManager = new SessionManager(mContext);

        if (sessionManager.isLoggedIn()) {

            AppGlobal.Tokan=sessionManager.getValuesSession(SessionManager.KEY_TOKEN);
            Log.d(TAG, "onCreate: "+AppGlobal.Tokan);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent loginIntent = new Intent(SplashActivity.this, FollowupbaseActivity.class);
                    loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(loginIntent);
                    finish();
                }
            }, 1000);

        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent loginIntent = new Intent(SplashActivity.this, LoginActivity.class);
                    loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(loginIntent);
                    finish();
                }
            }, 1000);
        }


    }


}

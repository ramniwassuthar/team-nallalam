package com.business.enquirymanagement.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.business.enquirymanagement.R;
import com.business.enquirymanagement.activity.FollowupbaseActivity;
import com.business.enquirymanagement.activity.LoginActivity;
import com.business.enquirymanagement.adapters.ProductListAdap;
import com.business.enquirymanagement.model.LoginResponse;
import com.business.enquirymanagement.model.ProductsListReponse;
import com.business.enquirymanagement.networks.ApiClient;
import com.business.enquirymanagement.networks.ApiInterface;
import com.business.enquirymanagement.networks.AppGlobal;
import com.business.enquirymanagement.networks.ProgressDialogLoader;
import com.business.enquirymanagement.useful.UtilityMethod;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Response;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by sunarctech on 3/7/18.
 */

public class ProductBaseFrag extends Fragment {

    ApiInterface apiInterface;
    private static final String TAG = "ProductBaseFrag";
    Context mContext ;
    RecyclerView recyclerView ;
    ImageView ivNoRecored;

    public static ProductBaseFrag newInstance(){
        ProductBaseFrag fragment = new ProductBaseFrag();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product_base, container, false);
        recyclerView = view.findViewById(R.id.recyclerView);
        ivNoRecored = view.findViewById(R.id.ivNoRecored);
        return view ;
    }

    @Override
    public void onAttach(Context context) {
        mContext = context ;
        super.onAttach(context);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }

    private void init() {
        Log.e(TAG,"inside init");


        ProgressDialogLoader.progressdialog_creation((Activity) mContext, mContext.getString(R.string.please_wait));

        apiInterface = ApiClient.getClientWithHeader().create(ApiInterface.class);
        Observable<Response<ProductsListReponse>> call = apiInterface.getProductsList();
        call.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ProductsListReponse>>() {
                    @Override
                    public void onCompleted() {
                        if (ProgressDialogLoader.isDalogShowing()) {
                            ProgressDialogLoader.progressdialog_dismiss();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (ProgressDialogLoader.isDalogShowing()) {
                            ProgressDialogLoader.progressdialog_dismiss();
                        }
                        UtilityMethod.showCoustomToastError(mContext, "");
                    }

                    @Override
                    public void onNext(Response<ProductsListReponse> response) {

                        try {
                            if (ProgressDialogLoader.isDalogShowing()) {
                                ProgressDialogLoader.progressdialog_dismiss();
                            }

                            if (response.isSuccessful()) {
                                if (response.body().getMeta().isSuccess()) {
                                    ivNoRecored.setVisibility(View.GONE);
                                    recyclerView.setVisibility(View.VISIBLE);

                                    LinearLayoutManager layoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
                                    ProductListAdap adap = new ProductListAdap(mContext,response.body().getData());
                                    recyclerView.setLayoutManager(layoutManager);
                                    recyclerView.setAdapter(adap);




                                } else {
                                    ivNoRecored.setVisibility(View.VISIBLE);
                                    recyclerView.setVisibility(View.GONE);
                                    UtilityMethod.showCoustomToastError(mContext, response.message());
                                }
                            } else {
                                ivNoRecored.setVisibility(View.VISIBLE);
                                recyclerView.setVisibility(View.GONE);
                                UtilityMethod.showCoustomToastError(mContext, "");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }
                });

    }




}

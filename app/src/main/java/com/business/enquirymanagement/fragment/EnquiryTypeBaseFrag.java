package com.business.enquirymanagement.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.business.enquirymanagement.R;
import com.business.enquirymanagement.activity.EnquirySubmitActivity;
import com.business.enquirymanagement.activity.LoginActivity;
import com.business.enquirymanagement.activity.ReportsActivity;
import com.business.enquirymanagement.adapters.SimpleFragmentPagerAdapter;
import com.business.enquirymanagement.useful.alert;

/**
 * Created by sunarctech on 13/6/18.
 */

public class EnquiryTypeBaseFrag extends Fragment {

    private static final String TAG = "EnquiryTypeBaseFrag";
    Context mContext ;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    TextView tvLogout, tvReport ;
    SimpleFragmentPagerAdapter adapter;
    FloatingActionButton fab;
    SearchView searchView ;

    private static final int ADD_REPORT = 3;



    public static EnquiryTypeBaseFrag newInstance(){
        EnquiryTypeBaseFrag fragment = new EnquiryTypeBaseFrag();
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_enquiry_base,container,false);
        tabLayout = (TabLayout) view.findViewById(R.id.sliding_tab);
        viewPager = (ViewPager) view.findViewById(R.id.viewPager);
        tvLogout = view.findViewById(R.id.tvLogout);
        tvReport = view.findViewById(R.id.tvReport);
        fab = view.findViewById(R.id.fab);


        Log.e(TAG,"onCreateView");

        return view;
    }

    @Override
    public void onAttach(Context context) {
        mContext = context ;
        super.onAttach(context);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }

    private void init(){

        adapter = new SimpleFragmentPagerAdapter(getActivity(),getChildFragmentManager());
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

        tvLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),LoginActivity.class);
                alert.show_ok_cancel_specific_with_clear_prefs(mContext,null,"Are you sure you want to logout ?",
                        intent);


            }
        });

        tvReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ReportsActivity.class);
                startActivity(intent);
            }
        });
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), EnquirySubmitActivity.class);
                startActivityForResult(intent,ADD_REPORT);
            }
        });




    }

    public void updateList(){

        adapter = new SimpleFragmentPagerAdapter(getActivity(),getChildFragmentManager());
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
       // super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == ADD_REPORT){
            if(resultCode == getActivity().RESULT_OK){
                String enquiryType = data.getStringExtra("enquiryType");
                Log.e(TAG,"onActivityResult==="+enquiryType);
                if(enquiryType.equals("HOT")){
                    tabLayout.getTabAt(0).select();
                }else if(enquiryType.equals("COLD")){
                    tabLayout.getTabAt(1).select();
                }else
                {
                    tabLayout.getTabAt(2).select();
                }
                adapter.notifyDataSetChanged();
            }
        }
    }

    /*private void checkListForNumber(String number,EnquiryM enquiryM){
       for(EnquiryM enquiryM1 :enquiryM.enquiryMSHOT){
           if(enquiryM1.mobileNo.contains(number)){
               tabLayout.getTabAt(0).select();
           }
       }

       for (EnquiryM enquiryM1 :enquiryM.enquiryMSCOLD){
           if(enquiryM1.mobileNo.contains(number)){
               tabLayout.getTabAt(1).select();
           }
       }

       for (EnquiryM enquiryM1 :enquiryM.enquiryMSWARM){
           if(enquiryM1.mobileNo.contains(number)){
               tabLayout.getTabAt(2).select();
           }
       }
    }*/
}


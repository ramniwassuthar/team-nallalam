package com.business.enquirymanagement.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.business.enquirymanagement.R;
import com.business.enquirymanagement.adapters.EnquiryTypeAdap;
import com.business.enquirymanagement.adapters.ProductListAdap;
import com.business.enquirymanagement.model.EnquiryM;
import com.business.enquirymanagement.model.ProductsListReponse;
import com.business.enquirymanagement.networks.ApiClient;
import com.business.enquirymanagement.networks.ApiInterface;
import com.business.enquirymanagement.networks.ProgressDialogLoader;
import com.business.enquirymanagement.useful.UtilityMethod;

import retrofit2.Response;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by sunarctech on 13/6/18.
 */

public class EnquiryTypeFrag extends Fragment {

    private static final String TAG = "EnquiryTypeFrag";
    Context mContext ;
    RecyclerView rvList;
    EnquiryM enquiryM ;
    String title ;
    View view;
    ApiInterface apiInterface;
    ImageView ivNoRecored;

    public static EnquiryTypeFrag newInstance(String title){
        EnquiryTypeFrag fragment = new EnquiryTypeFrag();
        Bundle bundle = new Bundle();
        bundle.putString("title",title);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         view = inflater.inflate(R.layout.fragment_recyclerview,container,false);
        title= getArguments().getString("title");
        Log.e(TAG,"ttile :- "+title);
        enquiryM = EnquiryM.newInstance();

        return view;
    }

    @Override
    public void onAttach(Context context) {
        mContext = context ;
        super.onAttach(context);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }

    private void init(){

        rvList = view.findViewById(R.id.rvList);
        ivNoRecored = view.findViewById(R.id.ivNoRecored);

        getList();

    }

    private void getList() {

        ProgressDialogLoader.progressdialog_creation((Activity) mContext, mContext.getString(R.string.please_wait));

        apiInterface = ApiClient.getClientWithHeader().create(ApiInterface.class);
        Observable<Response<ProductsListReponse>> call = apiInterface.getProductsList();
        call.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ProductsListReponse>>() {
                    @Override
                    public void onCompleted() {
                        if (ProgressDialogLoader.isDalogShowing()) {
                            ProgressDialogLoader.progressdialog_dismiss();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (ProgressDialogLoader.isDalogShowing()) {
                            ProgressDialogLoader.progressdialog_dismiss();
                        }
                        UtilityMethod.showCoustomToastError(mContext, "");
                    }

                    @Override
                    public void onNext(Response<ProductsListReponse> response) {

                        try {
                            if (ProgressDialogLoader.isDalogShowing()) {
                                ProgressDialogLoader.progressdialog_dismiss();
                            }

                            if (response.isSuccessful()) {
                                if (response.body().getMeta().isSuccess()) {
                                    ivNoRecored.setVisibility(View.GONE);
                                    rvList.setVisibility(View.VISIBLE);

                                    LinearLayoutManager layoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
                                    ProductListAdap adap = new ProductListAdap(mContext,response.body().getData());
                                    rvList.setLayoutManager(layoutManager);
                                    rvList.setAdapter(adap);




                                } else {
                                    ivNoRecored.setVisibility(View.VISIBLE);
                                    rvList.setVisibility(View.GONE);
                                    UtilityMethod.showCoustomToastError(mContext, response.message());
                                }
                            } else {
                                ivNoRecored.setVisibility(View.VISIBLE);
                                rvList.setVisibility(View.GONE);
                                UtilityMethod.showCoustomToastError(mContext, "");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }
                });
    }


}


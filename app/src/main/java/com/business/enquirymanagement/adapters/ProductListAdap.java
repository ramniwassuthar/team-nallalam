package com.business.enquirymanagement.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.business.enquirymanagement.R;
import com.business.enquirymanagement.activity.FollowupDetailActivity;
import com.business.enquirymanagement.model.ProductM;
import com.business.enquirymanagement.model.ProductsListReponse;
import com.business.enquirymanagement.networks.AppGlobal;

import java.util.List;

/**
 * Created by sunarctech on 3/7/18.
 */

public class ProductListAdap extends RecyclerView.Adapter<ProductListAdap.ViewHolder>     {
    private Context mContext;
    List<ProductsListReponse.DataBean> ProductsListReponse;

    private static final String TAG = "ProductListAdap";



    public ProductListAdap(Context mContext, List<ProductsListReponse.DataBean> data) {
        ProductsListReponse = data;
        this.mContext = mContext;

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_item, parent, false);
        return new ViewHolder(itemView);




    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {



        holder.tvBikeName.setText(ProductsListReponse.get(position).getName());
        holder.tvStatus.setText(ProductsListReponse.get(position).getStatus());
        holder.tvDescription.setText(ProductsListReponse.get(position).getDescription());

        Glide.with(mContext)
                .load(ProductsListReponse.get(position).getProduct_image())
                .into( holder.productImage);


     //   holder.valEngineCap.setText(ProductsListReponse.get(position).getName());

    }


    @Override
    public int getItemCount() {
        return ProductsListReponse.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public CardView relMain;
        private TextView tvBikeName,tvStatus,valMileage,valEngineCap,tvDescription;
        ImageView productImage ;

        public ViewHolder(View itemView) {
            super(itemView);
            relMain = itemView.findViewById(R.id.relMain);
            tvBikeName = itemView.findViewById(R.id.tvBikeName);
            tvStatus = itemView.findViewById(R.id.tvStatus);
            valMileage = itemView.findViewById(R.id.valMileage);
            productImage =itemView.findViewById(R.id.productImage);
            valEngineCap = itemView.findViewById(R.id.valEngineCap);
            tvDescription = itemView.findViewById(R.id.tvDescription);

        }
    }

}



package com.business.enquirymanagement.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import com.business.enquirymanagement.R;
import com.business.enquirymanagement.fragment.EnquiryTypeFrag;
import com.business.enquirymanagement.model.EnquiryM;

import java.util.ArrayList;

/**
 * Created by sunarctech on 8/6/18.
 */

public class SimpleFragmentPagerAdapter extends FragmentPagerAdapter {

    private Context mContext;

    private static final String TAG = "SimpleFragmentPagerAdap";
    EnquiryTypeFrag frag;


    public SimpleFragmentPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;


    }

    @Override
    public Fragment getItem(int position) {
        Log.e(TAG, "position==" + position);
        switch (position) {
            case 0:
                frag = EnquiryTypeFrag.newInstance(getPageTitle(position).toString());
                break;
            case 1:
                frag = EnquiryTypeFrag.newInstance(getPageTitle(position).toString());
                break;
            case 2:
                frag = EnquiryTypeFrag.newInstance(getPageTitle(position).toString());
                break;
        }

        return frag;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return mContext.getResources().getString(R.string.HOT);
            case 1:
                return mContext.getResources().getString(R.string.COLD);
            case 2:
                return mContext.getResources().getString(R.string.WARM);
        }
        return "";
    }



}

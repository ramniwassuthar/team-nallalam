package com.business.enquirymanagement.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.business.enquirymanagement.R;
import com.business.enquirymanagement.activity.FollowupDetailActivity;
import com.business.enquirymanagement.model.EnquiryM;
import com.business.enquirymanagement.model.ProductM;

import java.util.ArrayList;

/**
 * Created by sunarctech on 13/6/18.
 */

public class EnquiryTypeAdap extends RecyclerView.Adapter<EnquiryTypeAdap.ViewHolder>     {
    private ArrayList<EnquiryM> enquiryMS ;
    private Activity ctx;
    private String title ;


    private static final String TAG = "EnquiryTypeAdap";
    public static final int UPDATE_FOLLOWUP = 2 ;
    ProductM productM ;

    private ArrayList<String> proNames = new ArrayList<>();


    public EnquiryTypeAdap(Activity activity, String title, ArrayList<EnquiryM> enquiryMS) {
        this.enquiryMS = enquiryMS;
        this.ctx = activity;
        this.title= title;
        productM = ProductM.getInstance();

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.enquiry_type_item, parent, false);
        return new ViewHolder(itemView);




    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final EnquiryM enquiryM1 = enquiryMS.get(position);

          holder.tvCusName.setText(enquiryM1.customerName);
          holder.tvProDes.setText(enquiryM1.product);
          holder.valEnqType.setText(enquiryM1.enquiryType);
          holder.tvMob.setText("Mobile No : "+enquiryM1.mobileNo);

        for(ProductM productM : productM.productMS){
            proNames.add(productM.proName);
        }

       int index = proNames.indexOf(enquiryM1.product);
        Log.e(TAG,"index of product in proNames=="+index);


        if(index != -1) {
            int imageRes = productM.productMS.get(index).proImage;
            holder.productImage.setImageResource(imageRes);
            enquiryM1.proImage = imageRes ;
            enquiryMS.set(position,enquiryM1);
        }

        holder.relMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ctx, FollowupDetailActivity.class);
                Log.e(TAG,"enquiryM1.remark  onClick =="+enquiryM1.product);
                intent.putExtra("enquiryM1",enquiryM1);
                intent.putExtra("position",position);
                ctx.startActivityForResult(intent,UPDATE_FOLLOWUP);
            }
        });
    }


    @Override
    public int getItemCount() {
        return enquiryMS.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public RelativeLayout relMain;
        private TextView tvCusName,tvProDes,valEnqType,tvMob;
        private ImageView productImage ;

        public ViewHolder(View itemView) {
            super(itemView);
            relMain = itemView.findViewById(R.id.relMain);
            tvCusName = itemView.findViewById(R.id.tvCusName);
            tvProDes = itemView.findViewById(R.id.tvProDes);
            valEnqType = itemView.findViewById(R.id.valEnqType);
            tvMob = itemView.findViewById(R.id.tvMob);
            productImage = itemView.findViewById(R.id.productImage);



        }
    }

}


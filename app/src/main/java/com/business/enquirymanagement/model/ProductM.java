package com.business.enquirymanagement.model;

import java.util.ArrayList;

/**
 * Created by sunarctech on 3/7/18.
 */

public class ProductM {

    public ArrayList<ProductM> productMS = new ArrayList<>();

    public int proImage;
    public String proName ;
    public String proPrice;
    public String proMileage;
    public String engineCap ;

    public static ProductM instance = null ;

    public static ProductM getInstance(){
        if(instance ==null){
            instance = new ProductM();
        }
        return instance ;
    }

    public ProductM(){

    }

    public ProductM(int proImage,String proName ,String proPrice ,String proMileage,String engineCap){
        this.proImage = proImage;
        this.proMileage= proMileage;
        this.proName = proName ;
        this.proPrice = proPrice ;
        this.engineCap = engineCap ;

    }
}

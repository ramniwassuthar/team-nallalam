package com.business.enquirymanagement.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by sunarctech on 13/6/18.
 */

public class EnquiryM implements Serializable {

   public String customerName, address ,mobileNo, product,remark,enquiryType,activityPlace, followupDate,attempts;
    public String remark2 ="";
    public String followupDate2 ="" ;
    public String remark3= "";
    public String followupDate3 ="" ;
    public ArrayList<EnquiryM> enquiryMSHOT = new ArrayList<>();
    public ArrayList<EnquiryM> enquiryMSWARM = new ArrayList<>();
    public ArrayList<EnquiryM> enquiryMSCOLD = new ArrayList<>();

    public ArrayList<EnquiryM> totalEnquires = new ArrayList<>();
    public int proImage ;
    private static EnquiryM enquiryM;

   public static EnquiryM newInstance(){
       if(enquiryM ==null)
           enquiryM = new EnquiryM();
       return  enquiryM ;
   }

   private EnquiryM(){

   }
    public EnquiryM(String customerName,String address,String mobileNo,String product,String remark,String enquiryType ,String
                    activityPlace,String followupDate,String attempts){

        this.customerName = customerName ;
        this.address =address;
        this.mobileNo = mobileNo ;
        this.product = product ;
        this.remark = remark ;
        this.enquiryType = enquiryType ;
        this.activityPlace = activityPlace;
        this.followupDate = followupDate ;
        this.attempts = attempts ;
    }
}

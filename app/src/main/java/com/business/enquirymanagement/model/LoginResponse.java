package com.business.enquirymanagement.model;

public class LoginResponse {

    /**
     * meta : {"success":true,"code":null,"message":null}
     * data : {"token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjhlNGQ0MzdhNmQ3MDgxMzhlMWFkYmE1YjQ4OGU2MTYzYjE1ZTFhNWIzMTk3YTBkNzUxYWMxZjE3Mjc4NTYwNzhiNmFlNmRhZThhYWM3MDMwIn0.eyJhdWQiOiIzIiwianRpIjoiOGU0ZDQzN2E2ZDcwODEzOGUxYWRiYTViNDg4ZTYxNjNiMTVlMWE1YjMxOTdhMGQ3NTFhYzFmMTcyNzg1NjA3OGI2YWU2ZGFlOGFhYzcwMzAiLCJpYXQiOjE1MzQ4NDY3MzcsIm5iZiI6MTUzNDg0NjczNywiZXhwIjoxNTY2MzgyNzM3LCJzdWIiOiIyIiwic2NvcGVzIjpbXX0.DYycbG7V_nNCntehnUdybtPSJ1xMZTX7WeXYYAMyAyyCqZ6yw-g-BmUk92gj0krafKX6A4_Ri9msB0q4DsKZimuqEN3__L-lUtk6yqAvfPpDEO9auXR1JLt6vVRTLhiGcHGJuhyvyrNIBiInD87eP60HZF_VZmaIMOoV-RfjXds2ASDGI30fODF7aSP3OSfoT6c--6g3PN4mJOxBqsyKk2B55fx8dkNeuMO7EA3nwtJ-BfyPUx93qA66AxKOL5Yufy0LVaL1GVctm5q24XMuY5hvYfqiL7bILmtlHY2ooFyxsAG8okelzLbAKH5e53lm3sIjQXZhs2yirPqNqviD2GPRKmXfK5GAeX7Aw1NdkAgeUKKkwW0ejsTX3Kh3OXcSGVK3L81_Lk8gO2-D0NbEZy0UbIQGYa45sliL5p1HIn3K1l7BeK-Ob3Trlumhmfc_qdgLrVnUgmzBa5_mgYc66X7lfNQeeb2X1gF0b7Ld_RKWodMGM1WbNR5YzkdcVQ8nG1hQBlcSLyVz3RuuELawVa42NxG97fx6Xf12_mwVlb_sY-JgCuNMOeQ7Z7P03MgNBFaKfL06K40lwshJ3xJG6t85WTFBYp9aFDAXQQDYp9In8oYFMUIgneJtouUaFfXmuJCli8fUVLz0Ps2xYOMKORQoysTzRaUgu1H8x9Vil9E","name":"David","status":200}
     * timestamp : 1534846737
     * timezone : Asia/Kolkata
     */

    private MetaBean meta;
    private DataBean data;
    private int timestamp;
    private String timezone;

    public MetaBean getMeta() {
        return meta;
    }

    public void setMeta(MetaBean meta) {
        this.meta = meta;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public int getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(int timestamp) {
        this.timestamp = timestamp;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public static class MetaBean {
        /**
         * success : true
         * code : null
         * message : null
         */

        private boolean success;
        private Object code;
        private String message;

        public boolean isSuccess() {
            return success;
        }

        public void setSuccess(boolean success) {
            this.success = success;
        }

        public Object getCode() {
            return code;
        }

        public void setCode(Object code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * token : eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjhlNGQ0MzdhNmQ3MDgxMzhlMWFkYmE1YjQ4OGU2MTYzYjE1ZTFhNWIzMTk3YTBkNzUxYWMxZjE3Mjc4NTYwNzhiNmFlNmRhZThhYWM3MDMwIn0.eyJhdWQiOiIzIiwianRpIjoiOGU0ZDQzN2E2ZDcwODEzOGUxYWRiYTViNDg4ZTYxNjNiMTVlMWE1YjMxOTdhMGQ3NTFhYzFmMTcyNzg1NjA3OGI2YWU2ZGFlOGFhYzcwMzAiLCJpYXQiOjE1MzQ4NDY3MzcsIm5iZiI6MTUzNDg0NjczNywiZXhwIjoxNTY2MzgyNzM3LCJzdWIiOiIyIiwic2NvcGVzIjpbXX0.DYycbG7V_nNCntehnUdybtPSJ1xMZTX7WeXYYAMyAyyCqZ6yw-g-BmUk92gj0krafKX6A4_Ri9msB0q4DsKZimuqEN3__L-lUtk6yqAvfPpDEO9auXR1JLt6vVRTLhiGcHGJuhyvyrNIBiInD87eP60HZF_VZmaIMOoV-RfjXds2ASDGI30fODF7aSP3OSfoT6c--6g3PN4mJOxBqsyKk2B55fx8dkNeuMO7EA3nwtJ-BfyPUx93qA66AxKOL5Yufy0LVaL1GVctm5q24XMuY5hvYfqiL7bILmtlHY2ooFyxsAG8okelzLbAKH5e53lm3sIjQXZhs2yirPqNqviD2GPRKmXfK5GAeX7Aw1NdkAgeUKKkwW0ejsTX3Kh3OXcSGVK3L81_Lk8gO2-D0NbEZy0UbIQGYa45sliL5p1HIn3K1l7BeK-Ob3Trlumhmfc_qdgLrVnUgmzBa5_mgYc66X7lfNQeeb2X1gF0b7Ld_RKWodMGM1WbNR5YzkdcVQ8nG1hQBlcSLyVz3RuuELawVa42NxG97fx6Xf12_mwVlb_sY-JgCuNMOeQ7Z7P03MgNBFaKfL06K40lwshJ3xJG6t85WTFBYp9aFDAXQQDYp9In8oYFMUIgneJtouUaFfXmuJCli8fUVLz0Ps2xYOMKORQoysTzRaUgu1H8x9Vil9E
         * name : David
         * status : 200
         */

        private String token;
        private String name;
        private int status;

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }
    }
}

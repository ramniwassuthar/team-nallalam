package com.business.enquirymanagement.model;

import java.util.List;

public class ProductsListReponse {


    /**
     * meta : {"success":true,"code":null,"message":null}
     * data : [{"id":1,"name":"Splendor","product_image":"http://10.10.10.58:8000/product/images/no_image.png","status":"a","description":"Alloy wheels Splendor","deleted_at":null,"created_at":"2018-07-11 14:07:10","updated_at":"2018-07-11 14:07:10"},{"id":3,"name":"Pulsar","product_image":"http://10.10.10.58:8000/product/images/no_image.png","status":"a","description":"Alloy wheels Pulsar","deleted_at":null,"created_at":"2018-07-11 14:07:10","updated_at":"2018-07-11 14:07:10"},{"id":4,"name":"Cash","product_image":"http://10.10.10.58:8000/product/images/1531301024_logo-17.jpg","status":"a","description":"sdasdsad","deleted_at":null,"created_at":"2018-07-11 14:53:44","updated_at":"2018-07-11 14:53:44"}]
     * timestamp : 1535628007
     * timezone : Asia/Kolkata
     */

    private MetaBean meta;
    private int timestamp;
    private String timezone;
    private List<DataBean> data;

    public MetaBean getMeta() {
        return meta;
    }

    public void setMeta(MetaBean meta) {
        this.meta = meta;
    }

    public int getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(int timestamp) {
        this.timestamp = timestamp;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class MetaBean {
        /**
         * success : true
         * code : null
         * message : null
         */

        private boolean success;
        private Object code;
        private Object message;

        public boolean isSuccess() {
            return success;
        }

        public void setSuccess(boolean success) {
            this.success = success;
        }

        public Object getCode() {
            return code;
        }

        public void setCode(Object code) {
            this.code = code;
        }

        public Object getMessage() {
            return message;
        }

        public void setMessage(Object message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 1
         * name : Splendor
         * product_image : http://10.10.10.58:8000/product/images/no_image.png
         * status : a
         * description : Alloy wheels Splendor
         * deleted_at : null
         * created_at : 2018-07-11 14:07:10
         * updated_at : 2018-07-11 14:07:10
         */

        private int id;
        private String name;
        private String product_image;
        private String status;
        private String description;
        private Object deleted_at;
        private String created_at;
        private String updated_at;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getProduct_image() {
            return product_image;
        }

        public void setProduct_image(String product_image) {
            this.product_image = product_image;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public Object getDeleted_at() {
            return deleted_at;
        }

        public void setDeleted_at(Object deleted_at) {
            this.deleted_at = deleted_at;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }
    }
}

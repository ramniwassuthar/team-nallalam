package com.business.enquirymanagement.networks;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.view.Window;

import com.business.enquirymanagement.R;
import com.wang.avi.AVLoadingIndicatorView;

/**
 * Created by RN Suthar on 08-March-18.
 */
public class ProgressDialogLoader {
    private static Dialog dialog;
    static AVLoadingIndicatorView avi;


    public static void progressdialog_creation(Activity activity, String title) {
        try {
            dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().getDecorView().setBackgroundColor(Color.TRANSPARENT);

            dialog.setCancelable(false);
            dialog.setContentView(R.layout.dilog_progress);

            avi = dialog.findViewById(R.id.avi);
            avi.smoothToShow();

            dialog.show();

        } catch (Exception e) {

        }
    }

    public static void progressdialog_dismiss() {

        if ((dialog != null) && dialog.isShowing())
            // avi.smoothToHide();
            dialog.dismiss();

        dialog = null;


    }


    public static boolean isDalogShowing() {

        if ((dialog != null) && dialog.isShowing()) {
            return true;
        } else {
            return false;
        }


    }
}

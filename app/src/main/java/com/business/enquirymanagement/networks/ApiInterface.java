package com.business.enquirymanagement.networks;


import com.business.enquirymanagement.model.LoginResponse;
import com.business.enquirymanagement.model.ProductsListReponse;

import java.util.Map;

import retrofit2.Response;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by RN Suthar on 21-Aug-18.
 */

public interface ApiInterface {



    @FormUrlEncoded
    @POST("login")
    Observable<Response<LoginResponse>> login(@FieldMap Map<String, String> map);

    @GET("products")
    Observable<Response<ProductsListReponse>> getProductsList();

}




